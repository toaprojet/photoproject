package com.toa.jlm.ProjetToaJlm.Api;

import com.toa.jlm.ProjetToaJlm.Dto.PhotoDto;
import com.toa.jlm.ProjetToaJlm.Model.AlbumRepository;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Maxime on 14/05/2017.
 */
public class ApiPhoto {

    AlbumRepository albumRepository = AlbumRepository.getInstance();

    @RequestMapping(value = "/photo/postPhoto", method = RequestMethod.POST)
    public void postPhoto(@RequestBody PhotoDto photo){
        albumRepository.findOne(photo.getAlbumId()).ajouterPhoto(photo.getPhoto());
    }
    @RequestMapping(value = "/pc/deletePhoto", method = RequestMethod.POST)
    public void deletePhoto(@RequestBody PhotoDto photo){
        albumRepository.findOne(photo.getAlbumId()).supprPhoto(photo.getPhoto());
    }
    @RequestMapping(value = "/pc/addCommentaire", method = RequestMethod.POST)  // spec a definir
    public void AjouterCommentaire(@RequestBody PhotoDto photo){
        //albumRepository.findOne(photo.getAlbumId()).ajouterPhoto(photo.getPhoto());
    }

}
