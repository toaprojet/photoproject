package com.toa.jlm.ProjetToaJlm.Dto;

/**
 * Created by Maxime on 14/05/2017.
 */
public class UserAccessDto {
    int idUser;
    int idAlbum;

    public UserAccessDto() {
    }

    public UserAccessDto(int idUser, int idAlbum) {
        this.idUser = idUser;
        this.idAlbum = idAlbum;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdAlbum() {
        return idAlbum;
    }

    public void setIdAlbum(int idAlbum) {
        this.idAlbum = idAlbum;
    }
}
