package com.toa.jlm.ProjetToaJlm.Dto;

import com.toa.jlm.ProjetToaJlm.Model.Photo;

/**
 * Created by Maxime on 14/05/2017.
 */
public class PhotoDto {
    Photo photo ;
    int albumId;

    public PhotoDto(Photo photo, int albumId) {
        this.photo = photo;
        this.albumId = albumId;
    }

    public PhotoDto() {
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public int getAlbumId() {
        return albumId;
    }

    public void setAlbumId(int albumId) {
        this.albumId = albumId;
    }
}