package com.toa.jlm.ProjetToaJlm.Model;

/**
 * Created by Maxime on 14/05/2017.
 */
import java.util.Date;

public class Photo {

    //Attributs
    private int id;
    private String titre;	//Nom Image
    private String cheminImg; //Chemin de l'image
    private Date date;	//Date Image
    String localisatiion;
    private Commentaire com;

    //Constructeurs

    public Photo(){

    }


    Photo(int id ,String titre, String chemin, Date date, String localisatiion, Commentaire com){
        this.id = id;
        this.titre = titre;
        this.cheminImg = chemin;
        this.date = date;
        this.localisatiion = localisatiion;
        this.com = com;
    }

    Photo(Photo photo) {
        this.id = photo.getId();
        this.titre = photo.getTitre();
        this.cheminImg = photo.getChemin();
        this.date = photo.getDate();
        this.localisatiion=photo.localisatiion;
        this.com = photo.getCom();
    }

    //M�thodes

    //Assesseurs

    public String getTitre() {
        return this.titre;
    }

    public String getChemin() {
        return this.cheminImg;
    }

    public Date getDate() {

        return this.date;
    }

    public String getLocalisatiion() {
        return localisatiion;
    }

    public void setLocalisatiion(String localisatiion) {
        this.localisatiion = localisatiion;
    }

    public Commentaire getCom() {

        return this.com;
    }


    public int getId() {
        return id;
    }


    public void setId(int id) {

        this.id = id;
    }


    public String getCheminImg() {
        return cheminImg;
    }


    public void setCheminImg(String cheminImg) {

        this.cheminImg = cheminImg;
    }


    public void setTitre(String titre) {

        this.titre = titre;
    }


    public void setDate(Date date) {

        this.date = date;
    }


    public void setCom(Commentaire com) {

        this.com = com;
    }

    //Fin Assesseurs

}
