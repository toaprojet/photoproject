package com.toa.jlm.ProjetToaJlm.Model;

/**
 * Created by Maxime on 14/05/2017.
 */
public class Parametre {

    //Attributs

    boolean notif;	//Permet � l'utilisateur de d�terminer s'il souhaite les notifications ou non
    boolean vibre;	//Permet � l'utilisateur de d�terminer s'il souhaite les vibrations lors des notifications ou non

    //Constructeurs

    Parametre(){
        this.notif = true;
        this.vibre = true;
    }

    Parametre(boolean uneNotif, boolean vibration) {
        this.notif = uneNotif;
        this.vibre = vibration;
    }


	/*Assesseurs*/

    public boolean getNotif(){
        return this.notif;
    }

    public boolean getVibre(){
        return this.vibre;
    }


	/*Modifieurs*/

    public void setNotif(boolean uneNotif) {
        this.notif = uneNotif;
    }

    public void setVibre(boolean uneVibre) {
        this.vibre = uneVibre;
    }
}
