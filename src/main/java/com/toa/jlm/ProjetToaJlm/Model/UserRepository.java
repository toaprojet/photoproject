package com.toa.jlm.ProjetToaJlm.Model;

import java.util.List;

/**
 * Created by Maxime on 14/05/2017.
 */
public class UserRepository {

    // attributs

    private List<User> listUser;
    private static UserRepository INSTANCE;
    // constructeurs
    private UserRepository() {

    }

    public static UserRepository getInstance(){
        if( INSTANCE == null) {
            INSTANCE = new UserRepository();
        }
        return INSTANCE;
    }

    // get set
    public List<User> getListUser() {
        return listUser;
    }

    public void setListUser(List<User> listUser) {
        this.listUser = listUser;
    }

    // methodes

    public void creerUser(User user) {
        listUser.add(user);
        //push
    }

    public void removeUser(User user) {
        listUser.remove(user);
        //delete
    }


    public List<User> findAll() {
        return this.listUser;
        //majList
    }

    public User findOne(int id) {
        //majList
        for (User u : listUser) {
            if (u.getId() == id) {
                return u;
            }
        }
        return null;
    }

}