package com.toa.jlm.ProjetToaJlm.Model;

/**
 * Created by Maxime on 14/05/2017.
 */
public class Commentaire {
    //Attributs
    private int id;
    private int PhotoId;
    private String date;	//Date du Commentaire
    private String commentaire;
    //Constructeurs

    //Methodes


    public Commentaire(int id, int photoId, String date, String commentaire) {
        this.id = id;
        PhotoId = photoId;
        this.date = date;
        this.commentaire = commentaire;
    }

    public Commentaire() {
    }

    public int getPhotoId() {
        return PhotoId;
    }

    public void setPhotoId(int photoId) {
        PhotoId = photoId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}