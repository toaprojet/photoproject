package com.toa.jlm.ProjetToaJlm.Model;

/**
 * Created by Maxime on 14/05/2017.
 */
import java.util.List;

public class AlbumRepository {

    // attributs

    private  List<Album> listAlbum;
    private static AlbumRepository INSTANCE = null;

    // constructeurs
    private AlbumRepository() {

    }

    public static AlbumRepository getInstance(){
        if(INSTANCE == null){
            INSTANCE = new AlbumRepository();
        }
        return INSTANCE;
    }

    // get set
    public List<Album> getListAlbum() {
        return listAlbum;
    }

    public void setListAlbum(List<Album> listAlbum) {
        this.listAlbum = listAlbum;
    }

    // methodes

    public void addAlbum(Album album) {
        listAlbum.add(album);
    }

    public void removeAlbum(Album album) {
        listAlbum.remove(album);
    }

    public void removeAlbum(int id){
        Album temp = findOne(id);
        removeAlbum(temp);
    }

    public List<Album> findAll() {
        return this.listAlbum;
    }

    public Album findOne(int id) {
        for (Album a : listAlbum) {
            if (a.getId() == id) {
                return a;
            }
        }
        return null;
    }

}
