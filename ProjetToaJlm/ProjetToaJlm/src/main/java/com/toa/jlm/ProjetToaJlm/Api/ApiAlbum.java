package com.toa.jlm.ProjetToaJlm.Api;

import com.toa.jlm.ProjetToaJlm.Dto.UserAccessDto;
import com.toa.jlm.ProjetToaJlm.Model.Album;
import com.toa.jlm.ProjetToaJlm.Model.AlbumRepository;
import com.toa.jlm.ProjetToaJlm.Model.UserRepository;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * Created by Maxime on 14/05/2017.
 */
public class ApiAlbum {
    AlbumRepository albumRepository = AlbumRepository.getInstance();
    UserRepository userRepository = UserRepository.getInstance();

    @RequestMapping(value = "/album/postPhoto", method = RequestMethod.POST)
    public void postAlbum(@RequestBody Album album){
        albumRepository.addAlbum(album);
    }

    @RequestMapping(value = "/album/deletePhoto", method = RequestMethod.POST)
    public void deleteAlbum(@RequestBody int id){
        albumRepository.removeAlbum(id);
    }

    @RequestMapping(value = "/album/giveUserAccess", method = RequestMethod.POST)
    public void giveUserAcces(@RequestBody UserAccessDto userAcces){
        albumRepository.findOne(userAcces.getIdAlbum()).partagerAvec(userRepository.findOne(userAcces.getIdUser()));
    }
    public void removeUserAcces(@RequestBody UserAccessDto userAccessDto){
        albumRepository.findOne(userAccessDto.getIdAlbum()).annulerPartage(userRepository.findOne(userAccessDto.getIdUser()));
    }

}