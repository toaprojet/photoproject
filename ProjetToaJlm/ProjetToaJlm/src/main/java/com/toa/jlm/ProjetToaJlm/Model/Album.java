package com.toa.jlm.ProjetToaJlm.Model;

/**
 * Created by Maxime on 14/05/2017.
 */
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class Album extends Observable {

    //Attributs

    private int id;
    private String titre;	//Nom Album
    private String date;	//Date Album
    private List<User> listUserPartage;
    private User user;	//Utilisateur
    private List<Photo> photos;


    //Constructeurs

    Album(){

    }



    public Album(int id, String titre, String date,  User user) {
        this.id = id;
        this.titre = titre;
        this.date = date;
        this.listUserPartage = new ArrayList<>();
        this.user = user;
        this.photos = new ArrayList<>();
    }



    Album(Album album){
        this.id = album.getId();
        this.titre = album.getTitre();
        this.date = album.getDate();
        this.user = album.getUser();
        this.photos = album.getPhotos();
        this.listUserPartage = album.getListUserPartage();
    }


    //methodes









    //Assesseurs

    public List<Photo> getPhotos() {
        return photos;
    }

    public int getId() {
        return id;
    }



    public void setId(int id) {
        this.id = id;
    }



    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTitre() {
        return this.titre;
    }

    public String getDate() {
        return this.date;
    }

    public User getUser() {
        return this.user;
    }

    public List<User> getListUserPartage() {
        return listUserPartage;
    }

    public void setListUserPartage(List<User> listUserPartage) {
        this.listUserPartage = listUserPartage;
    }


    //Fin Assesseurs



    public void ajouterPhoto(Photo photo) {
        photos.add(photo);
        setChanged();
        notifyObservers("une photo a été ajouté ");
        //push
    }

    public void supprPhoto(Photo photo) {
        photos.remove(photo);
        //delete
    }

    public void partagerAvec(User user) {
        listUserPartage.add(user);
        addObserver(user);
        //push

    }


    public void annulerPartage(User user) {
        listUserPartage.remove(user);
        //delete
    }
}