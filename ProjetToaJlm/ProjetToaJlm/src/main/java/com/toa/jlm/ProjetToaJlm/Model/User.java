package com.toa.jlm.ProjetToaJlm.Model;

/**
 * Created by Maxime on 14/05/2017.
 */
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class User implements Observer {

    //Attributs
    private int id;

    private String pseudo;

    private String nom;

    private String prenom;

    private String email;

    private List<User> listContact;






    //Constructeurs

    public User(User user){
        this.id = user.id;
        this.pseudo = user.pseudo;
        this.nom = user.pseudo;
        this.prenom = user.prenom;
        this.email = user.email;
        this.listContact = user.listContact;
    }

    public User(int id,String pseudo, String nom, String prenom, String email) {
        this.id= id;
        this.pseudo = pseudo;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.listContact = new ArrayList<>();
    }

    public User(){

    }

//
public void update(Observable obs, Object obj) {
    if(obs instanceof Album){
        System.out.println("une photo a été ajouté a l'album :"+((Album) obs).getTitre());

    }
}


    //Get et set
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public List<User> getListContact() {
        return listContact;
    }
    public void setListContact(List<User> listContact) {
        this.listContact = listContact;
    }

    public String getPseudo() {
        return pseudo;
    }
    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public String getPrenom() {
        return prenom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    //M�thodes

    public void ajouterContact(User user){
        listContact.add(user);
        //push
    }

    public void retirerContact(User user){
        listContact.remove(user);
        //delete
    }


}
